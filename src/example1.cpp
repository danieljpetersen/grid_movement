#include "app.h"
#include "example1.h"

void vs::Example1::init()
{
    app->tick.deltaTime = 1.0 / 20.0;
    currentPosition = {0.0f, 0.0f};
    previousPosition = currentPosition;
}

////////////////////////////////////////////////////////////

void vs::Example1::step()
{
    previousPosition = currentPosition;

    if (app->input.leftArrow)
    {
        currentPosition.x -= moveAmount;
    }
    if (app->input.rightArrow)
    {
        currentPosition.x += moveAmount;
    }
    if (app->input.upArrow)
    {
        currentPosition.y -= moveAmount;
    }
    if (app->input.downArrow)
    {
        currentPosition.y += moveAmount;
    }
}

////////////////////////////////////////////////////////////

void vs::Example1::onUpdate()
{

}

////////////////////////////////////////////////////////////

void vs::Example1::onDraw()
{
    auto drawPosition = app->lerp(currentPosition, previousPosition);
    app->drawCircle(drawPosition, 20, sf::Color::Green);
}

////////////////////////////////////////////////////////////
