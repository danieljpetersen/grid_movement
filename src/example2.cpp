#include "app.h"
#include "example2.h"

void vs::Example2::init()
{
    app->tick.deltaTime = 0.29;
    for (int xIndex = 0; xIndex < xNumTiles; xIndex++)
    {
        for (int yIndex = 0; yIndex < yNumTiles; yIndex++)
        {
            Tile tile;
            tile.position.x = xIndex * tileSize + app->window.getSize().x/2 - (xNumTiles * tileSize)/2;
            tile.position.y = yIndex * tileSize + app->window.getSize().y/2 - (yNumTiles * tileSize)/2;
            tile.rowIndex = yIndex;
            tile.colIndex = xIndex;
            tiles.emplace_back(tile);
        }
    }
}

////////////////////////////////////////////////////////////

void vs::Example2::step()
{
    car.tileIndexPrevious = car.tileIndexCurrent;

    if (car.tileIndexCurrent != car.tileIndexGoal)
    {
        int rowCur = tiles[car.tileIndexCurrent].rowIndex;
        int colCur = tiles[car.tileIndexCurrent].colIndex;
        int rowGoal = tiles[car.tileIndexGoal].rowIndex;
        int colGoal = tiles[car.tileIndexGoal].colIndex;

        int rowNext = rowCur;
        int colNext = colCur;

        const int moveAmount = 1;
        if (rowGoal > rowCur)
        {
            rowNext += moveAmount;
        }
        else if (rowGoal < rowCur)
        {
            rowNext -= moveAmount;
        }

        else if (colGoal > colCur)
        {
            colNext += moveAmount;
        }
        else if (colGoal < colCur)
        {
            colNext -= moveAmount;
        }

        rowNext = std::min(rowNext, yNumTiles-1);
        rowNext = std::max(rowNext, 0);
        colNext = std::min(colNext, xNumTiles-1);
        colNext = std::max(colNext, 0);

        car.tileIndexCurrent = getIndex(colNext, rowNext);
    }
}

////////////////////////////////////////////////////////////

void vs::Example2::onUpdate()
{
    if (app->input.leftMouse)
    {
        for (int i = 0; i < tiles.size(); i++)
        {
            if (app->input.mouseWindowPosition.x >= tiles[i].position.x)
            {
                if (app->input.mouseWindowPosition.x < tiles[i].position.x + tileSize)
                {
                    if (app->input.mouseWindowPosition.y >= tiles[i].position.y)
                    {
                        if (app->input.mouseWindowPosition.y < tiles[i].position.y + tileSize)
                        {
                            car.tileIndexGoal = i;
                            break;
                        }
                    }
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////

void vs::Example2::onDraw()
{
    for (int i = 0; i < tiles.size(); i++)
    {
        app->drawRect(tiles[i].position.x, tiles[i].position.y+tileSize, tileSize-2, tileSize-2, sf::Color(30, 140, 30));
    }

    if (app->input.downArrow)
    {
        if (inputDebounce.getElapsedTime().asSeconds() > 0.3)
        {
            showSimulationPosition = !showSimulationPosition;
            inputDebounce.restart();
        }
    }

    // ----

    sf::Vector2f previousPosition = { tiles[car.tileIndexPrevious].position.x+tileSize/2.f, tiles[car.tileIndexPrevious].position.y+tileSize/2.f };
    sf::Vector2f currentPosition = { tiles[car.tileIndexCurrent].position.x+tileSize/2.f, tiles[car.tileIndexCurrent].position.y+tileSize/2.f };
    auto drawPosition = app->lerp(currentPosition, previousPosition);

    if (showSimulationPosition)
    {
        app->drawCircle(currentPosition, 12, sf::Color::Blue);
    }
    else
    {
        app->drawCircle(drawPosition, 12, sf::Color::Red);
    }
}

////////////////////////////////////////////////////////////

int vs::Example2::getIndex(int col, int row)
{
    auto index = (yNumTiles * col) + row;

    if (index >= xNumTiles*yNumTiles)
    {
        return -1;
    }
    if (index < 0)
    {
        return -1;
    }

    return index;
};

