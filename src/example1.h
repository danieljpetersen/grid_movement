#ifndef GRID_MOVEMENT_EXAMPLE1_H
#define GRID_MOVEMENT_EXAMPLE1_H

namespace vs {

class Example1 : public Plugin
{
public:
    void init() override;
    void step() override;
    void onUpdate() override;
    void onDraw() override;

    sf::Vector2f currentPosition, previousPosition;
    const float moveAmount = 10.0f;
};

}

#endif