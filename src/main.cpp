#include <chrono>
#include "app.h"
#include "example1.h"
#include "example2.h"

int main()
{
    vs::Example1 example1;
    vs::Example2 example2;
    vs::Application app;
    app.run({&example1, &example2});
    return 0;
}

////////////////////////////////////////////////////////////

void vs::Application::run(std::vector<Plugin*> plugins)
{
    this->plugins = plugins;

    auto seed = (unsigned)std::chrono::system_clock::now().time_since_epoch().count();
    srand(seed);
    rand.seed(seed);

    tick.currentTime = tick.clock.getElapsedTime().asSeconds();
    bool isFullscreen = true;
    window.create(sf::VideoMode::getDesktopMode(), appTitle, sf::Style::Fullscreen, sf::ContextSettings(0,0,16));
    window.setVerticalSyncEnabled(true);
    sf::RenderTexture guiCanvas;
    guiCanvas.create(window.getSize().x, window.getSize().y);
    font.loadFromFile("JetBrainsMonoNL-Medium.ttf");
    vertArrayTriangles.setPrimitiveType(sf::Triangles);

    for (int i = 0; i < plugins.size(); i++)
    {
        (*plugins[i]).app = this;
        (*plugins[i]).init();
    }

    int totalStepCount = 0;
    int totalFrameCount = 0;
    while (window.isOpen())
    {
        sf::Event event;
        bool fullscreenToggle = false;
        bool resized = false;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();

            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Left)
                {
                    input.leftArrow = true;
                }
                if (event.key.code == sf::Keyboard::Right)
                {
                    input.rightArrow = true;
                }
                if (event.key.code == sf::Keyboard::Up)
                {
                    input.upArrow = true;
                }
                if (event.key.code == sf::Keyboard::Down)
                {
                    input.downArrow = true;
                }

                if (event.key.code == sf::Keyboard::LBracket)
                {
                    previousPlugin();
                }
                if (event.key.code == sf::Keyboard::RBracket)
                {
                    nextPlugin();
                }
                if (event.key.code == sf::Keyboard::R)
                {
                    plugins[activePluginIndex]->init();
                }
                if (event.key.code == sf::Keyboard::Tilde)
                {
                    window.close();
                    return;
                }
                if (event.key.code == sf::Keyboard::Return)
                {
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt))
                    {
                        fullscreenToggle = true;
                    }
                }
                if (event.key.code == sf::Keyboard::Escape)
                {
                    if (isFullscreen)
                    {
                        fullscreenToggle = true;
                    }
                }
            }

            if (event.type == sf::Event::MouseButtonPressed)
            {
                if (event.mouseButton.button == sf::Mouse::Left)
                {
                    input.leftMouse = true;
                }
                if (event.mouseButton.button == sf::Mouse::Right)
                {
                    input.rightMouse = true;
                }
            }


            if (event.type == sf::Event::KeyReleased)
            {
                if (event.key.code == sf::Keyboard::Left)
                {
                    input.leftArrow = false;
                }
                if (event.key.code == sf::Keyboard::Right)
                {
                    input.rightArrow = false;
                }
                if (event.key.code == sf::Keyboard::Up)
                {
                    input.upArrow = false;
                }
                if (event.key.code == sf::Keyboard::Down)
                {
                    input.downArrow = false;
                }
            }

            if (event.type == sf::Event::MouseButtonReleased)
            {
                if (event.mouseButton.button == sf::Mouse::Left)
                {
                    input.leftMouse = false;
                }
                if (event.mouseButton.button == sf::Mouse::Right)
                {
                    input.rightMouse = false;
                }
            }


            if (event.type == sf::Event::Resized)
            {
                resized = true;
            }
        }

        Plugin *activePlugin = plugins[activePluginIndex];

        if (fullscreenToggle)
        {
            if (isFullscreen)
            {
                window.create({1920, 1080}, appTitle, sf::Style::Default, sf::ContextSettings(0, 0, 16));
                window.setVerticalSyncEnabled(true);
            }
            else
            {
                window.create(sf::VideoMode::getDesktopMode(), appTitle, sf::Style::Fullscreen, sf::ContextSettings(0,0,16));
                window.setVerticalSyncEnabled(true);
            }
            resized = true;
            isFullscreen = ! isFullscreen;
        }

        if (resized)
        {
            auto view = sf::View(sf::Vector2f(window.getSize().x/2.f, window.getSize().y/2.f), sf::Vector2f(window.getSize().x, window.getSize().y));
            window.setView(view);
            guiCanvas.create(window.getSize().x, window.getSize().y);
            activePlugin->init();
        }

        auto mouseWindowPosition2i = sf::Mouse::getPosition(window);
        input.mouseWindowPosition = sf::Vector2f(mouseWindowPosition2i.x, mouseWindowPosition2i.y);

        // ----
        totalFrameCount++;
        int stepCountThisFrame = 0;

        double newTime = tick.clock.getElapsedTime().asSeconds();
        double frameTime = newTime - tick.currentTime;
        tick.currentTime = newTime;
        tick.accumulator += frameTime;
        while (tick.accumulator >= tick.deltaTime)
        {
            stepCountThisFrame++;
            totalStepCount++;
            tick.accumulator -= tick.deltaTime;
            tick.time += tick.deltaTime;
            activePlugin->onUpdate();
            activePlugin->step();
        }

        if (stepCountThisFrame == 0)
        {
            activePlugin->onUpdate();
        }

        // ----

        window.clear(sf::Color(40, 40, 60));
        guiCanvas.clear(sf::Color::Transparent);

        activePlugin->onDraw();

        {
            sf::Text t;
            t.setFont(font);
            t.setString("Example " + std::to_string(activePluginIndex) + ", Frame Count " + std::to_string(totalFrameCount) + ", Step Count " + std::to_string(totalStepCount));
            t.setCharacterSize(24);
            t.setOutlineColor(sf::Color::Black);
            t.setOutlineThickness(1);
            t.setPosition(window.getSize().x/2.f - t.getGlobalBounds().width/2.f, 15);
            guiCanvas.draw(t);

            guiCanvas.display();
            sf::Sprite sprite;
            sprite.setTexture(guiCanvas.getTexture());
            window.draw(sprite);
        }

        window.draw(vertArrayTriangles);
        vertArrayTriangles.clear();
        window.display();
        glFinish(); // help prevent skips in windowed mode

        bool addFPSToTitle = true;
        if (addFPSToTitle)
        {
            std::stringstream ss;
            ss << appTitle << " - FPS: " << std::floor(1.0f / frameTime);
            window.setTitle(ss.str());
        }
    }
}

////////////////////////////////////////////////////////////

sf::Vector2f vs::Application::lerp(sf::Vector2f currentPosition, sf::Vector2f previousPosition, float startingFromNTicksAgo, float overTotalNumTicks)
{
    // the original from gaffer on games article
    //const double alpha = tick.accumulator / tick.deltaTime;

    // modified so that you can interpolate over a number of ticks rather than only over a single tick
    const double alpha = (tick.accumulator+tick.deltaTime*startingFromNTicksAgo) / (tick.deltaTime*overTotalNumTicks);

    sf::Vector2f Interpolated;
    Interpolated.x = currentPosition.x * alpha + previousPosition.x * ( 1.0 - alpha );
    Interpolated.y = currentPosition.y * alpha + previousPosition.y * ( 1.0 - alpha );
    return Interpolated;
}

////////////////////////////////////////////////////////////

void vs::Application::nextPlugin()
{
    std::cout << "OK " << std::endl;
    ++activePluginIndex;
    if (activePluginIndex >= plugins.size())
    {
        std::cout << "uh1 " << std::endl;
        activePluginIndex = 0;
        plugins[activePluginIndex]->init();
    }
}

////////////////////////////////////////////////////////////

void vs::Application::previousPlugin()
{
    std::cout << "OK2 " << plugins.size() << std::endl;
    --activePluginIndex;
    if (activePluginIndex < 0)
    {
        std::cout << "uh2 " << std::endl;
        activePluginIndex = plugins.size() - 1;
        plugins[activePluginIndex]->init();
    }
}

////////////////////////////////////////////////////////////

void vs::Application::drawLine(sf::Vector2f a, sf::Vector2f b, float thickness, sf::Color color)
{
    sf::Vector2f lineNormal(a.y - b.y, b.x - a.x);
    float _lineThickness = std::sqrt(lineNormal.x * lineNormal.x + lineNormal.y * lineNormal.y);
    if (_lineThickness != 0.f)
        lineNormal /= _lineThickness;

    lineNormal = lineNormal * thickness;
    lineNormal = lineNormal / 2.0f;

    auto aVert = sf::Vertex(a - lineNormal, color);
    auto bVert = sf::Vertex(a + lineNormal, color);
    auto cVert = sf::Vertex(b + lineNormal, color);
    auto dVert = sf::Vertex(b - lineNormal, color);

    vertArrayTriangles.append(aVert);
    vertArrayTriangles.append(bVert);
    vertArrayTriangles.append(dVert);
    vertArrayTriangles.append(dVert);
    vertArrayTriangles.append(bVert);
    vertArrayTriangles.append(cVert);
}

////////////////////////////////////////////////////////////

void vs::Application::drawCircle(sf::Vector2f centerPosition, float radius, sf::Color color)
{
    static const float pi = 3.141592654f;
    float numPoints = 100;

    auto getPoint = [&radius, &numPoints, &centerPosition](int pointIndex) -> sf::Vector2f {
        double angle = (2.0f * pi / (float)numPoints) * (float)pointIndex;
        float newX = (centerPosition.x + radius * std::cos(angle));
        float newY = (centerPosition.y + radius * std::sin(angle));
        return {newX, newY};
    };

    for (int pointIndex = 0; pointIndex < numPoints; pointIndex++)
    {
        auto a = getPoint(pointIndex);
        auto b = getPoint(pointIndex+1);

        vertArrayTriangles.append(sf::Vertex(centerPosition, color));
        vertArrayTriangles.append(sf::Vertex(a, color));
        vertArrayTriangles.append(sf::Vertex(b, color));
    }
}

////////////////////////////////////////////////////////////

void vs::Application::drawRect(float x, float y, float width, float height, sf::Color color)
{
    vertArrayTriangles.append(sf::Vertex({x, y}, color));
    vertArrayTriangles.append(sf::Vertex({x + width, y}, color));
    vertArrayTriangles.append(sf::Vertex({x, y + height}, color));

    vertArrayTriangles.append(sf::Vertex({x, y + height}, color));
    vertArrayTriangles.append(sf::Vertex({x + width, y}, color));
    vertArrayTriangles.append(sf::Vertex({x + width, y + height}, color));
}

int vs::Application::getRandomI(int min, int max)
{
    std::uniform_real_distribution<float> gen(min, max);
    return (int)gen(rand);
}

////////////////////////////////////////////////////////////

float vs::Application::getRandomF(float min, float max)
{
    std::uniform_real_distribution<float> gen(min, max);
    return gen(rand);
}

////////////////////////////////////////////////////////////
float vs::calculateDistance(sf::Vector2f a, sf::Vector2f b)
{
    return std::sqrt(pow(b.x - a.x, 2) + pow(b.y - a.y, 2));
}