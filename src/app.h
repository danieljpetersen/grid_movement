#ifndef GRID_MOVEMENT_H
#define GRID_MOVEMENT_H

#include <random>
#include <sstream>
#include <iostream>
#include <GL/gl.h>
#include <cmath>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

namespace vs {

struct Input
{
	bool leftArrow = false;
	bool rightArrow = false;
	bool upArrow = false;
	bool downArrow = false;
	bool leftMouse = false;
    bool rightMouse = false;
    sf::Vector2f mouseWindowPosition = {0,0};
};

////////////////////////////////////////////////////////////

struct Tick
{
	sf::Clock clock;
	double deltaTime = 0.0; // set per example
	double time = 0.0;
	double currentTime = 0.0;
	double accumulator = 0.0;
};

////////////////////////////////////////////////////////////

class Application;
class Plugin
{
public:
    Application *app=nullptr;
    virtual void init() = 0;
    virtual void step() = 0;
    virtual void onUpdate() = 0;
    virtual void onDraw() = 0;
};

////////////////////////////////////////////////////////////

class Application
{
public:
    std::string appTitle = "Untitled";
    sf::RenderWindow window;
    std::mt19937 rand;
    sf::Font font;
    Input input;
    Tick tick;

    int activePluginIndex = 0;
    std::vector<Plugin*> plugins;
    sf::VertexArray vertArrayTriangles;

    void run(std::vector<Plugin*> plugins);
    sf::Vector2f lerp(sf::Vector2f currentPosition, sf::Vector2f previousPosition, float startingFromNTicksAgo=0, float overTotalNumTicks=1);
    void drawLine(sf::Vector2f a, sf::Vector2f b, float thickness, sf::Color color);
    void drawCircle(sf::Vector2f centerPosition, float radius, sf::Color color);
    void drawRect(float x, float y, float width, float height, sf::Color color);

    int getRandomI(int min, int max);
    float getRandomF(float min, float max);

    void nextPlugin();
    void previousPlugin();
};

////////////////////////////////////////////////////////////

float calculateDistance(sf::Vector2f a, sf::Vector2f b);

} // namespace vs

#endif