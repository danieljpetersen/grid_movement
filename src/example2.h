#ifndef GRID_MOVEMENT_EXAMPLE2_H
#define GRID_MOVEMENT_EXAMPLE2_H

namespace vs {

class Tile
{
public:
    sf::Vector2f position;
    int rowIndex, colIndex;
};

////////////////////////////////////////////////////////////

class Car
{
public:
    int tileIndexCurrent=0, tileIndexPrevious=0, tileIndexGoal=0;
};

////////////////////////////////////////////////////////////

class Example2 : public Plugin
{
public:
    void init() override;
    void step() override;
    void onUpdate() override;
    void onDraw() override;
    int getIndex(int col, int row);

    sf::Clock inputDebounce;
    bool showSimulationPosition = false;
    const float tileSize = 32.0f;
    const int xNumTiles = 50;
    const int yNumTiles = 30;
    std::vector<Tile> tiles;
    Car car;

};

}

#endif